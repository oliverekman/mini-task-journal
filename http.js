const express = require('express');
const path = require('path');
const HOST = '0.0.0.0';
const { Sequelize, QueryTypes } = require('sequelize');
const cors = require('cors');

const app = express();
app.use(cors());

app.use(express.json());

// SEQUELIZE



if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});


// EXPRESS



// ------------AUTHOR---------- //
//----------GET----------------//
app.get('/v1/api/author', async (req, res)=> {
    
        try {
            console.log('Connection success');
            const author = await sequelize.query('SELECT * from author', { type: QueryTypes.SELECT});
            return res.status(200).json(author)
        }
        catch (e) {
            console.log(e);
        }
    
});
app.get('/v1/api/author/:id', async (req, res)=> {
    const { id } = req.params; 
    try {
        console.log('Connection success');
        const author = await sequelize.query('SELECT * from author where author_id = ?',{ replacements: [id], type: QueryTypes.SELECT});
        return res.status(200).json(author)
    }
    catch (e) {
        console.log(e);
    }

});


// ---------JOURNAL-------- //


app.post('/api/v1/journals', async (req, res) => {
    console.log(req.body.name);

    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }

    try {
      await sequelize.query(`INSERT INTO journal (name) VALUES ('${req.body.name}')`);

      return res.status(200).send(`Success!!!!`);
    } catch (e) {
      console.log(e);
    }
  });





app.get('/v1/api/journals', async (req, res)=> {
    
        try {
            console.log('Connection success');
            const journal = await sequelize.query('SELECT * from journal', { type: QueryTypes.SELECT});
            return res.status(200).json(journal)
        }
        catch (e) {
            console.log(e);
        }
         
});
app.get('/v1/api/journals/:id', async (req, res)=> {
    const { id } = req.params; 
    try {
        console.log('Connection success');
        const journalId = await sequelize.query('SELECT * from journal WHERE journal_id = ?', { replacements: [id], type: QueryTypes.SELECT});
        return res.status(200).json(journalId)
    }
    catch (e) {
        console.log(e);
    }
     
});
app.get('/v1/api/journals/:id/entries', async (req, res)=> {
    const { id } = req.params; 
    try {
        console.log('Connection success');
        const journalEntries = await sequelize.query('SELECT * FROM journal_entry WHERE journal_id = ?', { replacements: [id], type: QueryTypes.SELECT});
        return res.status(200).json(journalEntries)
    }
    catch (e) {
        console.log(e);
    }
     
});

// --------ENTRIES--------- //

app.post("/v1/api/entries", async (req, res) => {
    console.log(
      req.body.title,
      req.body.content,
      req.body.author_id,
      req.body.journal_id
    );
  
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
    try {
      console.log("Connection established...");
  
      await sequelize.query(
        `INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('${req.body.title}', '${req.body.content}', ${req.body.author_id}, ${req.body.journal_id})`
      );
      return res.status(200).send("Success!!");
    } catch (e) {
      console.error(e);
    }
  });
  



app.get('/v1/api/entries/', async (req, res)=> {
    try {
        console.log('Connection success');
        const entriesID = await sequelize.query(`SELECT je.journal_entry_id, 
		je.title, 
        je.content, 
        je.created_at, 
        CONCAT(a.first_name, ' ', a.last_name) AS author, 
                a.email AS author_email, 
                GROUP_CONCAT(t.name) AS tags
        FROM journal_entry AS je
        LEFT JOIN journal_entry_tag AS jt 
            USING(journal_entry_id)
        LEFT JOIN tag AS t 
            USING(tag_id)
        INNER JOIN author a 
            USING(author_id)
        GROUP BY je.title;`, {  type: QueryTypes.SELECT});
        return res.status(200).json(entriesID)
    }
    catch (e) {
        console.log(e);
    }
     
});


app.get('/v1/api/entries/:id', async (req, res)=> {
    const { id } = req.params; 
    try {
        console.log('Connection success');
        const entriesID = await sequelize.query('SELECT * from journal_entry WHERE journal_entry_id = ?', { replacements: [id], type: QueryTypes.SELECT});
        return res.status(200).json(entriesID)
    }
    catch (e) {
        console.log(e);
    }
     
});
app.get('/v1/api/entries/:id/tags', async (req, res)=> {
    const { id } = req.params; 
    try {
        console.log('Connection success');
        const entriesIDTag = await sequelize.query('SELECT * FROM journal_entry_tag WHERE journal_entry_id = ?', { replacements: [id], type: QueryTypes.SELECT});
        return res.status(200).json(entriesIDTag)
    }
    catch (e) {
        console.log(e);
    }
     
});

// -----------TAG----------- //

app.get('/v1/api/tags', async (req, res)=> {
    
    try {
        console.log('Connection success');
        const tags = await sequelize.query('SELECT * from tag', { type: QueryTypes.SELECT});
        return res.status(200).json(tags)
    }
    catch (e) {
        console.log(e);
    }
     
});
app.get('/v1/api/tags/:id', async (req, res)=> {
    const { id } = req.params; 
    try {
        console.log('Connection success');
        const tagsid = await sequelize.query('SELECT * from tag where tag_id = ?',{ replacements: [id], type: QueryTypes.SELECT})
        return res.status(200).json(tagsid)
    }
    catch (e) {
        console.log(e);
    }
     
});




const PORT = process.env.PORT || 5000;



app.listen(PORT, HOST);
console.log(`Server started on port ${PORT}`);



